#!/usr/bin/python
import os.path
import numpy as np


def load_UWGLA(flag_data, index=1):
    assert (flag_data == 'TRAIN') or (flag_data == 'TEST')
    assert (index > 0) and (index < 7)
    filename = 'UWGLA_' + flag_data
    x = np.loadtxt(filename, delimiter=',', dtype=float)[index-1, 1:]
    num_data = int(x.shape[0] / 3)
    return np.reshape(x, (num_data, 3))


def load_MHAD_Mocap(dirname, s, a, r, parts=None):
    """ load motion capture data of Berkeley MHAD dataset.
    Input:
        dirname: name of the directory where the target file is stored.
        s: Subject ID as int.
        a: Action ID as int.
        r: Repeat ID as int.
        parts: indices indicating which joint information is returned.
            Either None, int, or list of int.
            Return all the joint information with parts == None.
            Return a joint information with parts == int.
            Return joints information with parts == list of int.
    Return:
        x: The loaded time series of 3d position of joint(s) as numpy.array.
            The shape is (num_joint, num_frame, 3)
    """
    filename = "moc_s%02d_a%02d_r%02d.txt" % (s, a, r)
    assert os.path.isfile(filename)

    # delete the last 2 columns that are not position information
    _x = np.loadtxt(filename, delimiter=' ', dtype=float)[:, 0:-2]

    # reshape x of size (num_joint*3, num_frame) to (num_joint, num_frame, 3)
    num_joint = int(_x.shape[1] / 3)
    num_frame = _x.shape[0]
    x = np.zeros((num_joint, num_frame, 3))
    for i in range(num_joint):
        x[i, :, :] = _x[:, 3*i:3*(i+1)]

    # select if parts is specified
    if (type(parts) == int):
        assert (parts >= 0) and (parts < 43)
        x = x[parts, :, :]
    elif (type(parts) == list):
        assert (min(parts) >= 0) and (max(parts) < 43)
        x = x[parts, :, :]

    return x


def sample_from_data(x, skip=10):
    return x[list(range(0, len(x), skip)), :]


if __name__ == '__main__':
    x = load_MHAD_Mocap('./', 1, 2, 1, parts=None)
    x1 = load_MHAD_Mocap('./', 1, 2, 1, parts=3)
    x2 = load_MHAD_Mocap('./', 1, 2, 1, parts=[1, 4, 6])
