#!/usr/bin/python
from sklearn.metrics.pairwise import manhattan_distances

from load_data import load_MHAD_Mocap, sample_from_data
from shapedtw import matching_shapedtw
from visualizer import show_correspondences


if __name__ == '__main__':
    dir_mocap = './'
    id_subject = [1, 2]
    id_action = 2
    id_repeat = 1
    parts = 3
    for dim in range(1, 4):
        # load MHAD dataset
        x_train = load_MHAD_Mocap(dir_mocap,
                                  id_subject[0],
                                  id_action,
                                  id_repeat,
                                  parts=parts)[:, 0:dim]
        x_test = load_MHAD_Mocap(dir_mocap,
                                 id_subject[1],
                                 id_action,
                                 id_repeat,
                                 parts=parts)[:, 0:dim]
        x_train = sample_from_data(x_train, skip=10)
        x_test = sample_from_data(x_test, skip=10)

        dist, correspondences = matching_shapedtw(x_train,
                                                  x_test,
                                                  manhattan_distances)
        show_correspondences(x_train, x_test, correspondences)
