#!/usr/bin/python
import numpy as np

from load_data import load_MHAD_Mocap, sample_from_data
from shape_context import find_ranges
from shape_context import compute_shape_context
from visualizer import show_shape_context


if __name__ == '__main__':
    parts = 3
    for dim in range(1, 4):
        # load MHAD dataset
        x_train = load_MHAD_Mocap('./', 1, 2, 1, parts=parts)[:, 0:dim]
        x_test = load_MHAD_Mocap('./', 2, 2, 1, parts=parts)[:, 0:dim]
        x_train = sample_from_data(x_train)
        x_test = sample_from_data(x_test)

        # compute shape context
        ranges = find_ranges(np.concatenate((x_train, x_test)))
        v_train = compute_shape_context(x_train, ranges)
        v_test = compute_shape_context(x_test, ranges)

        # show shape context
        show_shape_context(v_train)
        show_shape_context(v_test)
