from math import sqrt, pi
import numpy as np
from scipy.spatial.distance import pdist, squareform

import matplotlib.pyplot as plt


class ShapeContext1d():
    def __init__(self, num_bins_distance=5):
        self.bins = [num_bins_distance]

    def compute(self, p):
        assert p.ndim == 1

        num_data = p.shape[0]
        D = squareform(pdist(np.vstack((p.flatten(), p.flatten())).transpose(), 'euclidean') + np.finfo(np.float32).eps)
        np.fill_diagonal(D, np.finfo(np.float32).eps)
        mat_distance = np.log(D)
        mat_feature = np.zeros((num_data, self.bins[0]))
        for i in range(num_data):
            mat_feature[i, :] = np.histogram(mat_distance[i, :],
                                             bins=self.bins[0])[0].astype(np.float32)
        return mat_feature


class ShapeContext2d():
    def __init__(self,
                 num_bins_distance=5,
                 num_bins_direction=12):
        self.bins = [num_bins_distance, num_bins_direction]

    def compute(self, p):
        assert p.ndim == 2 and p.shape[1] == 2

        num_data = p.shape[0]
        D = squareform(pdist(p, metric='euclidean'))
        print(D.shape)
        np.fill_diagonal(D, np.finfo(np.float32).eps)
        print(D.shape)
        mat_distance = np.log(D)
        print(mat_distance.shape)
        # mat_direction = pdist(p, metric=lambda u, v: np.arccos(np.clip(np.dot(u, v), -1.0, 1.0)))
        mat_direction = squareform(pdist(p, metric=lambda u, v: np.arctan2(v[1]-u[1], v[0]-u[0])))
        np.fill_diagonal(mat_direction, np.finfo(np.float32).eps)

        mat_feature = np.zeros((num_data, self.bins[0], self.bins[1]))
        for i in range(num_data):
            mat_feature[i, :, :] = np.histogram2d(mat_distance[i, :], mat_direction[i, :], bins=self.bins)
        return mat_feature


class ShapeContext3d():
    def __init__(self,
                 num_bins_distance=5,
                 num_bins_direction=12,
                 num_bins_elevation=6):
        self.bins = [num_bins_distance, num_bins_direction, num_bins_elevation]

    def compute(self, p):
        assert p.ndim == 2 and p.shape[1] == 3

        num_data = p.shape[0]
        mat_distance = np.log(pdist(p, metric='euclidean'))
        mat_direction = pdist(p[:, 0:2], metric=lambda u, v: np.arctan2(v[1]-u[1], v[0]-u[0]))
        mat_elevation = pdist(p, metric=lambda u, v: np.arctan2(v[2]-u[2], np.sqrt((v[0]-u[0])**2 + (v[1]-u[1])++2)))

        mat_feature = np.zeros((num_data, self.bins[0], self.bins[1], self.bins[2]))
        for i in range(num_data):
            mat_samples = np.concatenate(mat_distance[i, :], mat_direction[i, :], mat_elevation[i, :])
            mat_feature[i, :, :, :] = np.histogramdd(mat_samples, bins=self.bins)
        return mat_feature


class ShapeContext():
    def __init__(self, dim):
        self.dim = dim
        self.set_param()

    def set_param(self,
                  num_bins_distance=5,
                  num_bins_direction=12,
                  num_bins_elevation=6):
        if self.dim == 1:
            self.descriptor = ShapeContext1d(num_bins_distance)
        elif self.dim == 2:
            self.descriptor = ShapeContext2d(num_bins_distance,
                                             num_bins_direction)
        elif self.dim == 3:
            self.descriptor = ShapeContext3d(num_bins_distance,
                                             num_bins_direction,
                                             num_bins_elevation)
        else:
            self.descriptor = None

    def compute(self, p):
        if p.ndim == 1:
            assert self.dim == 1
        else:
            assert p.shape[1] == self.dim

#        return self.descriptor.compute(p.astype(np.float32))
        return self.descriptor.compute(p)


def compute_pairwise_distance(x, dim):
    if dim == 1:
        _distance_pair = squareform(pdist(np.vstack((x.flatten(), x.flatten())).transpose(), 'euclidean'))
    else:
        _distance_pair = squareform(pdist(x, 'euclidean'))

    shape = _distance_pair.shape
    distance_pair = np.zeros((shape[0], shape[1]-1))
    for n in range(shape[0]):
        distance_pair[n, :] = np.delete(_distance_pair[n, :], n)

    return distance_pair


def compute_pairwise_angle_azimuth(x):
    num_data = m.shape[0]
    # compute azimuth angle ranging [-pi, pi]
    azimuth_pair = np.zeros((num_data, num_data-1))
    for n in range(num_data):
        x_norm = np.delete(x - x[n, :], n, axis=0)
        azimuth_pair[n, :] = np.arctan2(x_norm[:, 1], x_norm[:, 0])
#        print('%s -> %s' % (np.arctan2(x_norm[:, 1], x_norm[:, 0])), np.arctan2(x_norm[:, 1], x_norm[:, 0]))

    # convert range from [-pi, pi] to [0, 2pi]
    azimuth_pair[azimuth_pair < 0.0] += 2.0*np.pi

    return azimuth_pair


def compute_pairwise_angle_elevation(x):
    num_data = m.shape[0]
    # compute elevation angle ranging [-pi/2, pi/2]
    elevation_pair = np.zeros((num_data, num_data-1))
    for n in range(num_data):
        x_norm = np.delete(x - x[n, :], n, axis=0)
        x_distance = np.linalg.norm(x_norm, axis=1)
        elevation_pair[n, :] = np.arccos(np.divide(x_norm[:, 2], x_distance))

    return elevation_pair


def compute_shape_context_1d(x, number_of_bins):
    num_data = x.shape[0]
    distance_pair = compute_pairwise_distance(x, 1)
    min_dist = max(np.min(distance_pair), np.finfo(np.float32).eps)
    max_dist = np.max(distance_pair)
    bins = np.logspace(np.log2(min_dist),
                       np.log2(max_dist),
                       num=number_of_bins+1,
                       base=2.0,
                       endpoint=True).tolist()
    shape_context = np.zeros((num_data, number_of_bins))
    for i in range(num_data):
        shape_context[i, :] = np.histogram(distance_pair[i, :],
                                           bins=bins)[0].astype(np.float32)

    return shape_context


def compute_shape_context_2d(x, number_of_bins):
    num_data = x.shape[0]
    dim = x.shape[1]
    distance_pair = compute_pairwise_distance(x, dim)
    min_dist = max(np.min(distance_pair), np.finfo(np.float32).eps)
    max_dist = np.max(distance_pair)
    direction_pair = compute_pairwise_angle_azimuth(x)

    # set bins
    # bins[0] for distance
    # bins[1] for direction
    bins_dist = np.logspace(np.log2(min_dist),
                            np.log2(max_dist),
                            num=number_of_bins[0]+1,
                            base=2.0,
                            endpoint=True).tolist()
    bins = (bins_dist, number_of_bins[1])
    shape_context = np.zeros((num_data,
                              number_of_bins[0],
                              number_of_bins[1]))
    for i in range(num_data):
        shape_context[i, :, :] = np.histogram2d(distance_pair[i, :],
                                                direction_pair[i, :],
                                                bins=bins)[0].astype(np.float32)

    return shape_context


def compute_shape_context_3d(x, number_of_bins):
    num_data = x.shape[0]
    dim = x.shape[1]
    distance_pair = compute_pairwise_distance(x, dim)
    min_dist = max(np.min(distance_pair), np.finfo(np.float32).eps)
    max_dist = np.max(distance_pair)
    # azimuth angle is computed from x, y coordinates
    direction_azimuth_pair = compute_pairwise_angle_azimuth(x[:, 0:2])
    # azimuth angle is computed from x, z coordinates
    direction_elevation_pair = compute_pairwise_angle_azimuth(x[:, [0, 2]])

    # set bins
    # bins[0] for distance
    # bins[1] for direction
    bins_dist = np.logspace(np.log2(min_dist),
                            np.log2(max_dist),
                            num=number_of_bins[0]+1,
                            base=2.0,
                            endpoint=True).tolist()
    bins = (bins_dist, number_of_bins[1], number_of_bins[2])
    shape_context = np.zeros((num_data,
                              number_of_bins[0],
                              number_of_bins[1],
                              number_of_bins[2]))
    for i in range(num_data):
        shape_context[i, :, :, :] = np.histogramdd(distance_pair[i, :],
                                                   direction_azimuth_pair[i, :],
                                                   direction_elevation_pair[i, :],
                                                   bins=bins)[0].astype(np.float32)

    return shape_context


def show_shape_context_1d(shape_contexts):
    xedges = list(range(shape_contexts[0].shape[0]))
    yedges = list(range(shape_contexts[0].shape[1]))
    grid_x, grid_y = np.meshgrid(xedges, yedges)

    num_of_shape_context = len(shape_contexts)

    if num_of_shape_context == 1:
        plt.title('feature reference')
        plt.xlabel('index of points')
        plt.ylabel('frequency')
        plt.pcolormesh(grid_x, grid_y, shape_contexts[0].transpose())
        plt.colorbar()
    else:
        for n in range(num_of_shape_context):
            plt.subplot(1, num_of_shape_context, n+1)
            plt.title('feature')
            plt.xlabel('index of points')
            plt.ylabel('frequency')
            plt.pcolormesh(grid_x, grid_y, shape_contexts[n].transpose())
            plt.colorbar()

    plt.show()


def show_shape_context_2d(shape_contexts):
    xedges = list(range(shape_contexts[0].shape[0]))
    yedges = list(range(shape_contexts[0].shape[1]))
    zedges = list(range(shape_contexts[0].shape[2]))
    grid_x, grid_y, grid_z = np.meshgrid(xedges, yedges, zedges)

    num_of_shape_context = len(shape_contexts)

    if num_of_shape_context == 1:
        plt.title('feature reference')
        plt.xlabel('index of points')
        plt.ylabel('frequency')
        plt.pcolormesh(grid_x, grid_y, grid_z, shape_contexts[0].transpose())
        plt.colorbar()
    else:
        for n in range(num_of_shape_context):
            plt.subplot(1, num_of_shape_context, n+1)
            plt.title('feature')
            plt.xlabel('index of points')
            plt.ylabel('frequency')
            plt.pcolormesh(grid_x, grid_y, grid_z, shape_contexts[n].transpose())
            plt.colorbar()

    plt.show()


if __name__ == '__main__':
    """ 2d """
    m = np.array([[0, 0],
       [0, 1],
       [1, 1],
       [1, 0],
       [0.5, 0.5]])
    m_angle_2d = compute_pairwise_angle_azimuth(m) * 180.0 / np.pi

    """ 3d """
    m = np.array([[0, 0, 0],
                  [0, 0, 1],
                  [0, 1, 0],
                  [0, 1, 1],
                  [1, 0, 0],
                  [1, 0, 1],
                  [1, 1, 0],
                  [1, 1, 1],
                  [0.5, 0.5, 0.5]])
    num_data = m.shape[0]
    """
    m_azi[n, m] for azimuth angle of m-th point against n-th point
    m_ele[n, m] for elevation angle of m-th point against m-th point
    """
    m_azi = np.zeros((num_data, num_data-1))
    m_ele = np.zeros((num_data, num_data-1))
    for n in range(num_data):
        m_norm = np.delete(m - m[n, :], n, axis=0)
        m_distance = np.linalg.norm(m_norm, axis=1)
        m_azi[n, :] = np.arctan2(m_norm[:, 1], m_norm[:, 0]) * 180 / np.pi
        m_ele[n, :] = np.arccos(np.divide(m_norm[:, 2], m_distance)) * 180 / np.pi

    m_angle2 = np.zeros((num_data, num_data-1, 2))
    m_angle2[:, :, 0] = compute_pairwise_angle_azimuth(m)
    m_angle2[:, :, 1] = compute_pairwise_angle_elevation(m)
#    m_angle2 = compute_pairwise_angle_azimuth(m)

    
    
#        m_cossim = 1-squareform(pdist(m+np.finfo(np.float32).eps, 'cosine'))
#    m_angle = 360*(1/3.14)*np.arccos(m_cossim)

#    np.random.seed()
#    # data
#    num_data = 20
#    scale = 10
#
#    for dim in range(2, 3):
#        # descriptor
#        descriptor = ShapeContext(dim)
#
#        # generate data
#        x = None
#        y = None
#        if dim == 1:
#            x = scale * np.random.rand(num_data)
#            y = x + np.random.uniform(-1.0, 1.0, num_data)
#        else:
#            x = scale * np.random.rand(num_data, dim)
#            y = x + 0.1 * np.random.uniform(-1.0, 1.0, (num_data, dim))
#
#        # compute shape context
#        if dim == 1:
#            """
#            10 bins for log-direction
#            """
#            bins = 10
#            x_sc = compute_shape_context_1d(x, bins)
#            y_sc = compute_shape_context_1d(y, bins)
#            show_shape_context_1d([x_sc, y_sc])
#        elif dim == 2:
#            """
#            10 bins for log-direction
#            8 bins for in-plane angle
#            """
#            bins = [10, 8]
#            x_sc = compute_shape_context_2d(x, bins)
#            y_sc = compute_shape_context_2d(y, bins)
#            show_shape_context_1d([x_sc[3, :, :]])
#        elif dim == 3:
#            """
#            10 bins for log-direction
#            8 bins for in-plane angle
#            4 bins for elevation angle
#            """
#            bins = [10, 8, 4]
#            x_sc = compute_shape_context_3d(x, bins)
#            y_sc = compute_shape_context_3d(y, bins)
#            show_shape_context_1d([x_sc[3, :, :, :]])
#            
