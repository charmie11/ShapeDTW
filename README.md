# ShapeDTW

implement ShapeDTW, proposed in the following paper, with python.
shapeDTW: shape Dynamic Time Warping
Jiaping Zhao, Laurent Itti
https://arxiv.org/abs/1606.01601

The authors open their MatLab implementation at https://github.com/jiapingz/shapeDTW/tree/master/shapeDTW
